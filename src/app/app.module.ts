import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';


import { AppComponent } from './app.component';
import { ConfigService } from './common/config.service';
import { HttpClientModule } from '@angular/common/http';

export function app_init(configService: ConfigService) {
  return () => configService.load();
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    ConfigService,
    {
      'provide': APP_INITIALIZER,
      'useFactory': app_init,
      'deps': [ConfigService],
      'multi': true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
