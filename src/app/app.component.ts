import { Component } from '@angular/core';
import { ConfigService } from './common/config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(private config: ConfigService) {
    this.title = config.getConfigProperty('baseUrl');
  }
}
