import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class ConfigService {
  private config: any;
  private loaded: false;

  constructor(private http: HttpClient) { }

  load() {
    const configPromise = new Promise((resolve, reject) => {
      this.http.get('config/config.json').subscribe(data => {
        this.config = data;
        resolve();
      });
    });
    return configPromise;
  }

  getConfigProperty(property: string): any {
    return this.config[property];
  }
}
